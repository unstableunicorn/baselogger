from setuptools import setup 

setup(
        name='baselogger',
        py_modules=['baselogger'],
        version='1.0.0',
        url='',
        license='MIT',
        author='elric.hindy',
        author_email='elric.hindy@seeingmachines.com',
        description='A logger class containing file, console and file&console loggers'
)
